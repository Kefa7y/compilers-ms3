
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Sun May 06 01:38:18 EET 2018
//----------------------------------------------------

import java_cup.runtime.*;
import java.io.*;
import java.util.*;

/** CUP v0.11a beta 20060608 generated parser.
  * @version Sun May 06 01:38:18 EET 2018
  */
public class parser extends java_cup.runtime.lr_parser {

  /** Default constructor. */
  public parser() {super();}

  /** Constructor which sets the default scanner. */
  public parser(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public parser(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\023\000\002\002\005\000\002\002\004\000\002\002" +
    "\002\000\002\007\005\000\002\010\005\000\002\010\002" +
    "\000\002\003\003\000\002\003\003\000\002\012\004\000" +
    "\002\012\003\000\002\013\004\000\002\013\004\000\002" +
    "\004\015\000\002\005\010\000\002\005\003\000\002\006" +
    "\004\000\002\006\002\000\002\011\004\000\002\011\002" +
    "" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\053\000\014\002\uffff\003\ufff1\031\ufff1\073\ufff1\076" +
    "\006\001\002\000\010\003\015\031\013\073\016\001\002" +
    "\000\004\002\012\001\002\000\020\002\uffef\003\uffef\031" +
    "\uffef\073\uffef\076\010\116\uffef\117\uffef\001\002\000\020" +
    "\002\ufff2\003\ufff2\031\ufff2\073\ufff2\076\ufff2\116\ufff2\117" +
    "\ufff2\001\002\000\020\002\uffef\003\uffef\031\uffef\073\uffef" +
    "\076\010\116\uffef\117\uffef\001\002\000\020\002\ufff0\003" +
    "\ufff0\031\ufff0\073\ufff0\076\ufff0\116\ufff0\117\ufff0\001\002" +
    "\000\004\002\000\001\002\000\006\010\051\062\052\001" +
    "\002\000\006\002\ufff1\076\006\001\002\000\016\002\ufff8" +
    "\003\ufff8\031\ufff8\073\ufff8\076\ufff8\117\ufff8\001\002\000" +
    "\004\031\027\001\002\000\016\002\ufffa\003\ufffa\031\ufffa" +
    "\073\ufffa\076\ufffa\117\ufffa\001\002\000\016\002\ufff1\003" +
    "\ufff1\031\ufff1\073\ufff1\076\006\117\ufff1\001\002\000\016" +
    "\002\ufffb\003\ufffb\031\ufffb\073\ufffb\076\ufffb\117\ufffb\001" +
    "\002\000\016\002\ufffc\003\015\031\013\073\016\076\ufffc" +
    "\117\ufffc\001\002\000\010\002\ufffe\076\ufffe\117\ufffe\001" +
    "\002\000\016\002\ufff1\003\ufff1\031\ufff1\073\ufff1\076\006" +
    "\117\ufff1\001\002\000\016\002\ufffc\003\015\031\013\073" +
    "\016\076\ufffc\117\ufffc\001\002\000\010\002\ufffd\076\ufffd" +
    "\117\ufffd\001\002\000\004\047\030\001\002\000\004\031" +
    "\031\001\002\000\004\006\032\001\002\000\004\053\033" +
    "\001\002\000\004\011\034\001\002\000\004\053\035\001" +
    "\002\000\004\007\036\001\002\000\004\005\037\001\002" +
    "\000\010\003\041\076\006\116\ufff1\001\002\000\004\116" +
    "\043\001\002\000\016\002\ufff3\003\ufff3\031\ufff3\073\ufff3" +
    "\076\ufff3\117\ufff3\001\002\000\016\002\ufff5\003\ufff5\031" +
    "\ufff5\073\ufff5\076\ufff5\117\ufff5\001\002\000\012\003\ufff1" +
    "\031\ufff1\073\ufff1\076\006\001\002\000\010\003\015\031" +
    "\013\073\016\001\002\000\006\076\006\117\ufff1\001\002" +
    "\000\004\117\047\001\002\000\016\002\ufff4\003\ufff4\031" +
    "\ufff4\073\ufff4\076\ufff4\117\ufff4\001\002\000\004\002\001" +
    "\001\002\000\006\003\015\031\013\001\002\000\004\031" +
    "\054\001\002\000\016\002\ufff9\003\ufff9\031\ufff9\073\ufff9" +
    "\076\ufff9\117\ufff9\001\002\000\016\002\ufff7\003\ufff7\031" +
    "\ufff7\073\ufff7\076\ufff7\117\ufff7\001\002\000\016\002\ufff6" +
    "\003\ufff6\031\ufff6\073\ufff6\076\ufff6\117\ufff6\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\053\000\006\002\004\006\003\001\001\000\012\003" +
    "\017\004\016\007\013\012\020\001\001\000\002\001\001" +
    "\000\004\011\006\001\001\000\002\001\001\000\004\011" +
    "\010\001\001\000\002\001\001\000\002\001\001\000\004" +
    "\013\052\001\001\000\004\006\047\001\001\000\002\001" +
    "\001\000\002\001\001\000\002\001\001\000\004\006\021" +
    "\001\001\000\002\001\001\000\012\003\023\004\016\010" +
    "\022\012\020\001\001\000\002\001\001\000\004\006\024" +
    "\001\001\000\012\003\023\004\016\010\025\012\020\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\002\001\001\000\002\001\001\000\006" +
    "\005\041\006\037\001\001\000\002\001\001\000\002\001" +
    "\001\000\002\001\001\000\004\006\043\001\001\000\012" +
    "\003\017\004\016\007\044\012\020\001\001\000\004\006" +
    "\045\001\001\000\002\001\001\000\002\001\001\000\002" +
    "\001\001\000\004\012\054\001\001\000\002\001\001\000" +
    "\002\001\001\000\002\001\001\000\002\001\001" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$parser$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$parser$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$parser$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 1;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}

}

/** Cup generated class to encapsulate user supplied action code.*/
class CUP$parser$actions {
  private final parser parser;

  /** Constructor */
  CUP$parser$actions(parser parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$parser$do_action(
    int                        CUP$parser$act_num,
    java_cup.runtime.lr_parser CUP$parser$parser,
    java.util.Stack            CUP$parser$stack,
    int                        CUP$parser$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$parser$result;

      /* select the action based on the action number */
      switch (CUP$parser$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 18: // newlineOP ::= 
            {
              Boolean RESULT =null;
		RESULT = true;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("newlineOP",7, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 17: // newlineOP ::= NEWLINE newlineOP 
            {
              Boolean RESULT =null;
		int nleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int nright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean n = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = n;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("newlineOP",7, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 16: // newline ::= 
            {
              Boolean RESULT =null;
		RESULT = true;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("newline",4, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 15: // newline ::= NEWLINE newlineOP 
            {
              Boolean RESULT =null;
		int nleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int nright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean n = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = n;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("newline",4, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 14: // suite ::= error 
            {
              Boolean RESULT =null;
		RESULT = false;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("suite",3, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 13: // suite ::= newline INDENT newline statements newline DEDENT 
            {
              Boolean RESULT =null;
		int nleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-5)).left;
		int nright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-5)).right;
		Boolean n = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-5)).value;
		int n1left = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)).left;
		int n1right = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-3)).right;
		Boolean n1 = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-3)).value;
		int ssleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).left;
		int ssright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).right;
		Boolean ss = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-2)).value;
		int n2left = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).left;
		int n2right = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).right;
		Boolean n2 = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-1)).value;
		
            RESULT = ss;
            
              CUP$parser$result = parser.getSymbolFactory().newSymbol("suite",3, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-5)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 12: // for_stmt ::= FOR ID IN ID OPARAN NUMBER COMMA NUMBER CPARAN COLON suite 
            {
              Boolean RESULT =null;
		int sleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int sright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean s = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		

              RESULT = s;
              
              CUP$parser$result = parser.getSymbolFactory().newSymbol("for_stmt",2, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-10)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // expr_list ::= EQUAL expr 
            {
              Boolean RESULT =null;
		int eleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int eright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean e = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = e;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("expr_list",9, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // expr_list ::= ASTRICK ID 
            {
              Boolean RESULT =null;
		RESULT = true;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("expr_list",9, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // expr ::= error 
            {
              Boolean RESULT =null;
		RESULT = false;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("expr",8, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // expr ::= ID expr_list 
            {
              Boolean RESULT =null;
		int elleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int elright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean el = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		RESULT = el;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("expr",8, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // statement ::= for_stmt 
            {
              Boolean RESULT =null;
		int fsleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int fsright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean fs = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		
			  RESULT = fs;
			  
              CUP$parser$result = parser.getSymbolFactory().newSymbol("statement",1, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // statement ::= expr 
            {
              Boolean RESULT =null;
		int eleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int eright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean e = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		
			  RESULT = e;
			  
              CUP$parser$result = parser.getSymbolFactory().newSymbol("statement",1, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // statementsO ::= 
            {
              Boolean RESULT =null;
		
                RESULT = true;
                
              CUP$parser$result = parser.getSymbolFactory().newSymbol("statementsO",6, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // statementsO ::= statement newline statementsO 
            {
              Boolean RESULT =null;
		int sleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).left;
		int sright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).right;
		Boolean s = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-2)).value;
		int soleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int soright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean so = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		
                  RESULT = so && s;
                  
              CUP$parser$result = parser.getSymbolFactory().newSymbol("statementsO",6, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // statements ::= statement newline statementsO 
            {
              Boolean RESULT =null;
		int sleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).left;
		int sright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).right;
		Boolean s = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-2)).value;
		int ssleft = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int ssright = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean ss = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		
              RESULT = s && ss;
              
              CUP$parser$result = parser.getSymbolFactory().newSymbol("statements",5, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // program ::= 
            {
              Boolean RESULT =null;
		
			RESULT = true;
			
              CUP$parser$result = parser.getSymbolFactory().newSymbol("program",0, ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // $START ::= program EOF 
            {
              Object RESULT =null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).right;
		Boolean start_val = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-1)).value;
		RESULT = start_val;
              CUP$parser$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$parser$parser.done_parsing();
          return CUP$parser$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // program ::= newline statements newline 
            {
              Boolean RESULT =null;
		int nleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).left;
		int nright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)).right;
		Boolean n = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-2)).value;
		int eleft = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).left;
		int eright = ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-1)).right;
		Boolean e = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.elementAt(CUP$parser$top-1)).value;
		int n1left = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).left;
		int n1right = ((java_cup.runtime.Symbol)CUP$parser$stack.peek()).right;
		Boolean n1 = (Boolean)((java_cup.runtime.Symbol) CUP$parser$stack.peek()).value;
		
			RESULT = e;
			
              CUP$parser$result = parser.getSymbolFactory().newSymbol("program",0, ((java_cup.runtime.Symbol)CUP$parser$stack.elementAt(CUP$parser$top-2)), ((java_cup.runtime.Symbol)CUP$parser$stack.peek()), RESULT);
            }
          return CUP$parser$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number found in internal parse table");

        }
    }
}

